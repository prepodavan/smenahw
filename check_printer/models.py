from django.db import models
from django.contrib.postgres.fields import JSONField


class Printer(models.Model):
    name = models.CharField(max_length=1000)

    # access key for API. Uniquely identifies printer.
    api_key = models.CharField(max_length=1000, unique=True)

    # type of checks which can be printed.
    # kitchen | client
    check_type = models.CharField(max_length=7)

    # point id where printer is located in.
    point_id = models.IntegerField()

    def __str__(self):
        # required for readable view in admin
        return f'{self.__class__.__name__}({self.id}, {self.name})'


class Check(models.Model):
    printer_id = models.ForeignKey(Printer, on_delete=models.DO_NOTHING)

    # kitchen | client
    type = models.CharField(max_length=7)
    order = JSONField()

    # new | rendered | printed
    status = models.CharField(max_length=8)
    pdf_file = models.FileField()

    def __str__(self):
        # required for readable view in admin
        return f'{self.__class__.__name__}({self.id})'
