from django.apps import AppConfig


class CheckPrinterConfig(AppConfig):
    name = 'check_printer'
