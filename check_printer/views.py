import os
import json
from .models import Printer, Check
from .pdfcheck import enqueue_render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.http import require_POST, require_GET


def make_json_error(code: int, message: str):
    """
    Shortcut for creating {'error': msg} response with given status code.
    :param code: `int`, status code.
    :param message: `str`, containing message of the error.
    :return: `JsonResponse`
    """
    response = JsonResponse(
        {'error': message}
    )
    response.status_code = code
    return response


@csrf_exempt
@require_POST
def create_checks(request):
    """
    Django View for checks creating by given order.
    :param request: Body of the json should contain json with "id" and "point_id" fields.
    :return: `JsonResponse` with status of operation and explanation,
    code = 400 either if no printers for given point or checks for order with given id already created.
    """
    order = json.loads(request.body)

    # find printers for given point
    printers = Printer.objects.filter(point_id=order['point_id'])

    # if no printers - error
    if len(printers) < 1:
        return make_json_error(400, "Для данной точки не настроено ни одного принтера")

    # check if checks with given order already created
    existed_checks = Check.objects.filter(
        printer_id=printers[0].id
    ).filter(
        order__id=order['id']
    )

    if len(existed_checks) != 0:
        return make_json_error(400, "Для данного заказа уже созданы чеки")

    # create new checks with type of the printer
    # and push render task
    for printer in printers:
        check = Check.objects.create(
            printer_id=printer,
            type=printer.check_type,
            order=order,
            status='new',
        )
        check.save()

        enqueue_render(check)

    return JsonResponse({'ok': 'Чеки успешно созданы'})


@csrf_exempt
@require_GET
def new_checks(request):
    """
    Django View for selecting new rendered checks for given printer
    identified by api_key.
    :param request: 'api_key' query string.
    :return: `JsonResponse` with status of operation and explanation,
    code = 401 if no printer with given api_key.
    """
    try:
        # check availability of the api_key and a printer with given api_key.
        api_key = request.GET['api_key']
        printer = Printer.objects.get(api_key=api_key)
    except (Printer.DoesNotExist, MultiValueDictKeyError) as exc:
        return make_json_error(401, "Ошибка авторизации")

    # find rendered and not printed checks by printer
    checks = {'checks': list()}
    for check in Check.objects.filter(printer_id=printer):
        if check.status == 'rendered':
            checks['checks'].append({'id': check.id})
    return JsonResponse(checks)


@csrf_exempt
@require_GET
def get_check(request):
    """
    Django View for getting pdf of check by given check_id.
    Requires authorization by api_key.
    :param request: 'api_key' query string, `check_id` query string.
    :return:`JsonResponse` with status of operation and explanation,
    code = 401 if no printer with given api_key.
    code = 400 either if check with given id does not exist
    or pdf file wasn't generated.
    """
    try:
        # check availability of the api_key and a printer with given api_key.
        api_key = request.GET['api_key']
        printer = Printer.objects.get(api_key=api_key)
    except (Printer.DoesNotExist, MultiValueDictKeyError) as exc:
        return make_json_error(401, "Ошибка авторизации")

    try:
        # find check by id
        # default = -1 to be sure Check.DoesNotExist will be raised
        check_id = request.GET.get('check_id', -1)
        check = Check.objects.filter(
            printer_id=printer
        ).get(pk=check_id)
    except Check.DoesNotExist as exc:
        return make_json_error(400, "Данного чека не существует")

    if check.status != 'rendered' and check.status != 'printed':
        return make_json_error(400, "Для данного чека не сгенерирован PDF-файл")

    response = HttpResponse(check.pdf_file.read(), content_type='application/pdf')

    # to force browsers show file as pdf, not a bunch of bytes
    response['Content-Disposition'] = f'filename="{os.path.basename(check.pdf_file.name)}"'
    check.status = 'printed'
    check.save()
    return response

