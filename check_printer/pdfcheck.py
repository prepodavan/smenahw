import json
import base64
import requests
import tempfile
import django_rq
from pathlib import Path
from .models import Check
from django.conf import settings
from django.core.files import File
from django.template.loader import render_to_string


PDF_DIR = Path(settings.MEDIA_ROOT) / 'pdf'
TMP_DIR = Path(tempfile.gettempdir())
QUEUE_NAME = 'checks'


def enqueue_render(check: Check):
    """
    Push render task to redis queue with given `Check`.
    :param check: `Check` to be rendered.
    :return:
    """
    django_rq.get_queue(QUEUE_NAME).enqueue(render, check.id)


def render(check_id: int):
    """
    Synchronously render the `Check` taken by given id
    with template corresponding to `type` of the check.
    Rendered checks stored at the MEDIA/pdf folder
    with name <id>_<type>.pdf
    :param check_id: `int`, id of the check to be rendered.
    :return:
    """
    check = Check.objects.get(pk=check_id)
    html = render_to_string(f'{check.type}_check.html', check.order)

    # Django FieldFile can not save location of existing file.
    # So let's generate pdf into temp file and then
    # let Django create new permanent file in needed folder
    tmp = temp_filepath(check)
    perm = permanent_filepath(check)

    to_pdf(html, tmp)

    file = File(tmp.open('rb'))
    check.pdf_file.save(str(perm.resolve()), file)

    # delete temp file
    tmp.unlink()

    check.status = 'rendered'
    check.save()


def filename(check: Check):
    return f'{check.order["id"]}_{check.type}.pdf'


def permanent_filepath(check: Check):
    return PDF_DIR / filename(check)


def temp_filepath(check: Check):
    return TMP_DIR / filename(check)


def to_pdf(html: str, output_path_like, input_encoding='utf-8', output_encoding='utf-8'):
    """
    Render given html using wkhtmltopdf-aas and save pdf into given location.
    :param html: `str` containing html.
    :param output_path_like: `os.PathLike` object pointing to destination of rendered pdf.
    :param input_encoding: `str` containing encoding of given html (default: utf-8)
    :param output_encoding: `str` containing encoding of resulting pdf (default: utf-8)
    :return:
    """

    # wkhtmltopdf-aas requires base64 encoded string
    data = {
        'contents': base64.b64encode(bytes(html, input_encoding)).decode('ascii'),
        'options': {
            'encoding': output_encoding
        }
    }

    headers = {
        'Content-Type': 'application/json',    # This is important
    }

    response = requests.post(settings.WKHTMLTOPDF['URL'], data=json.dumps(data), headers=headers)

    with output_path_like.open('wb') as f:
        f.write(response.content)

