from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):

    """
    Create default admin account if not any admin account exists.
    """

    def handle(self, *args, **options):
        for user in settings.ADMINS.values():

            username = user['USERNAME']

            if User.objects.filter(username=username).count() != 0:
                print(f'Admin with username "{username}" already exist')
                continue

            email = user['EMAIL']
            password = user['PASSWORD']
            print(f'Creating account for {username} ({email})')
            admin = User.objects.create_user(email=email, username=username, password=password)
            admin.is_superuser = True
            admin.is_staff = True
            admin.save()

