FROM python:latest
RUN mkdir /code
RUN mkdir /code/media
RUN mkdir /code/media/pdf
WORKDIR /code
COPY reqs.txt /code/
RUN pip install -r reqs.txt
COPY . /code/