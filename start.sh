#!/bin/bash

sleep 20
python manage.py makemigrations
python manage.py migrate
python manage.py initadmin
python manage.py loaddata initial_data
python manage.py rqworker checks &
python manage.py runserver 0.0.0.0:8000
